# Представления

Представления (views - ВИД) - это "тип" веб-страницы, который отвечает за специфичную функцию и имеет специфичный шаблон. Например, для блога ты мог бы воспользоваться следующими видами:

- Blog homepage - высвечивает последние записи;

- Entry "detail" page - детальные данные одной записи;

- Year-based archive page - записи за год;

- Comment action - пост комментариев на заданную запись.

В нашем опросе у нас будут следующие представления:

- Question index page - последние несколько опросов;

- Question detail page - форма для голосования;

- Question results page - форма для отображения результатов;

- Vote action - осуществляет голосование для конкретного выбора в конкретном вопросе.

## URL-паттерны

В Django, веб страницы и другой контент предоставляются представлениями (Views). Каждое представление является простой Python функцией (или методом). Django выбирает представление по URL, который запрошен (точнее - по части URL после доменного имени). 

Наверное, в сети вы встречали URL вроде `ME2/Sites/dirmod.asp?sid=&type=gen&mod=Core+Pages&gid=A6CD4967199A42D9B65B1B`. Django имеет более элегантные паттерны, чем это.

URL-паттерн это просто URL вроде `/news/year/month`. 

Чтобы получить представление из URL, Django использует URLconfs. Эта конструкция маппит URL-паттерны к видам.

## Пишем новые представления

Добавим новые представления в polls/views.py. Они немного отличаются от прошлых, ведь принимают аргумент question_id:

```python
def detail(resuest, question_id):
    return HttpResponse(f'Looking at question {question_id} details.')

def results(request, question_id):
    return HttpResponse(f'Looking at question {question_id} results.')

def vote(request, question_id):
    return HttpResponse(f'Voting on question {question_id}.')
```

Теперь добавляем их в polls/urls.py:

```python
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:question_id>/', views.detail, name='detail'),
    path('<int:question_id>/results/', views.results, name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]

```
Сам polls был привязан в mysite/urls.py в старом туториале.

Теперь можно вводить запросы вида `http://127.0.0.1:8000/polls/1/vote/`. Это будет вызывать соответствующие функции из views.py.

## Пояснение за URL

Когда кто-то просит страницу с твоего сайта, например, `/polls/34/`, Django загрузит mysite.urls Python модуль, потому что он указан в ROOT_URLCONF setting. Он найдет переменную urlpatterns и обработает их по порядку. Когда попадётся элемент `polls/`, он отрежет часть `polls/` и передаст в `polls.urls` только оставшуюся часть - `34/`. Там оно совпадёт с `<int:question_id>/` и вызовет detail() модуль.

Кстати, штука умеет и дополнять. Например, `/polls/34` тоже сработает.

Использование скобочек `<>` позволяет переслать паттерн как аргумент функции. Первая часть означает конвертер (в int), а вторая - имя именной переменной. Таким образом, мы получим следующий вызов при `polls/34` и `<int:question_id>`:

`views.detail(request, question_id=34)`

## Виды, которые что-то делают

Каждое представление ответственно за что-то:

- Вернуть HttpResponse объект, содержащий контент для запрошенной страницы;

- Вернуть исключение вроде Http404.

Представления могут читать записи БД или нет, использовать шаблоны Django или сторонние библиотеки, генерировать PDF, отдавать XML, создавать ZIP-архивы на лету и вообще делать всё, что умеет Python.

А вот Django требует возврата HttpResponse или эксепшена.

Попробуем использовать нашу БД. Перезапишем index() вид:

```python
from .models import Question

def index( request ):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    output = ', '.join([q.question_text for q in latest_question_list])
    return HttpResponse(output)

```

Теперь страничка index читает из БД сущности Question (последние пять штук) и выводит их через запятую.

Вот тут возникает проблем. Дизайн страницы захардкожен в представлении. Это неправильно - каждое представление будет возить свой дизайн. Нужно воспользоваться системой шаблонов Django чтобы разделить дизайн и Python-код.

## Отделение дизайна

Для начала нужна директория polls/templates. Тут Django будет искать шаблоны.

Настройка TEMPLATES указывает как Django будет загружать и рендерить шаблоны. Дефолтные настройки конфигурируют DjangoTemplates бекенд, чей APP_DIRS флаг установлен в True. По соглашению DjangoTemplates ищет шаблоны в каталоге templates для каждого приложения из INSTALLED_APPS.

В каталоге templates нужно создать каталог polls, а внутри - файл index.html. Шаблон будет лежать по пути `polls/templates/polls/index.html`. Но на него можно ссылаться через polls/index.html (по причине того, как app_directories template loader работает).

Папка polls нужна для неймспейсинга (ведь index-ов в проекте может быть много).

Разместим такой код в index.html:

```html
{% if latest_question_list %}
    <ul>
    {% for question in latest_question_list %}
        <li><a href="/polls/{{ question.id }}/">{{ question.question_text }}</a></li>
    {% endfor %}
    </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}
```

Затем такой код в polls/views.py:

```python
from django.http import HttpResponse
from django.template import loader
from .models import Question


def index( request ):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list':latest_question_list,
    }
    return HttpResponse(template.render(context, request))
```

Код загрузит шаблон index.html. Контекст - это словарь, маппящий имена переменных шаблона к объектам Python (создаёт ссылку на детали вопроса и добавляет поверх текст вопроса).

### Шорткат render()

Загрузить шаблон, вписать контекст и отдать HttpResponse с заполненным (отрендеренным) шаблоном - это частая задача в Web. Поэтому Django предоставляет сокращение. Вот как будет выглядеть переписанный код:

```python
def index( request ):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]

    context = {
        'latest_question_list':latest_question_list,
    }

    return render(request, 'polls/index.html', context)
```

В приницпе, теперь нет смысла импортировать HttpResponse и loader, потому что это делает шорткат render.


## Возврат ошибки 404

Теперь переработаем функцию detail.

```python
from django.http import Http404

def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404(f'Question {question_id} does not exist')
    return render(request, 'polls/detail.html', {'question':question})
```

И добавим шаблон templates/polls/detail.html

```html
{{ question }}
```

Теперь попытка получить несуществующий ID из БД заканчивается страничкой 404.

### Шорткат get_object_or_404()

Для этого действия тоже есть шорткат. Можно переписать это следующим образом:

```python
def detail(request, question_id):
    # Если такого PrimaryKey не существует, вернётся ошибка
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question':question})
```

Функция get_object_or_404 прнимает на вход объект (таблицу) и первичный ключ, который из него нужно достать.
Очевидно, больше не нужен импорт Http404.

Кстати, для filter() существует функция get_list_or_404(), которая возвращает Http404, если ничего в таблице не подошло к фильтру.


# Использование системы шаблонов

Поменяем шаблон details:

```html
<h1>{{ question.question_text }}</h1>
<ul>
{% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }}</li>
{% endfor %}
</ul>
```

Благодаря теперь шаблон напрямую использует методы Python, чтобы получать из объектов БД поля. Как это работает:

- Django просматривает словарь и ищет объект question (его мы передаём);

- Django просматривает атрибуты объекта;

- Вызов метода происходит в скобках {% %}. question.choice_set.all интерпретируется как pyhton код question.choice_set.all(), который вернёт iterable Choice лист.

## Избавление от хардкодных URL в шаблонах

Когда мы писали шаблон polls/index.html, ссылка была захардкоженна таким образом:

```html
<li><a href="/polls/{{ question.id }}/">{{ question.question_text }}</a></li>
```

Проблема здесь в том, что если в проекте много шаблонов, становится невозможным изменять URL. Однако, мы ведь уже используем именные аргументы *name* в path() в polls.urls:

```python
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:question_id>/', views.detail, name='detail'),
    path('<int:question_id>/results/', views.results, name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
```

Их же можно исползовать в шаблонах:


```html
<li><a href="{% url 'detail' question.id %}">{{ question.question_text }}</a></li>
```

Таким образом, URL берётся из файла polls.urls, а затем к нему прибавляется id из переданного question. Теперь достаточно поменять его там, чтобы задействовать в шаблонах автоматически.


## Неймспейсы в именах URL

Предположим, в одном проекте есть два файла urls, в каждом из которых есть ссылка с именем 'details'. Как между ними выбрать при вызове {% url %}?

Для этого нужно добавить namespace в URLconf. За это отвечает переменная app_name в polls.urls:

```python
app_name='polls'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:question_id>/', views.detail, name='detail'),
    path('<int:question_id>/results/', views.results, name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]

```

Теперь можно уточнить, для какого приложения вызывать путь:

```html
<li><a href="{% url 'polls:detail' question.id %}">{{ question.question_text }}</a></li>
```
