from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from .models import Question

def index( request ):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {
        'latest_question_list':latest_question_list,
    }
    # возвращает HttpRespinse с заполненным шаблоном polls/index с помощью контекста
    return render(request, 'polls/index.html', context)

def detail(request, question_id):
    # Если такого PrimaryKey не существует, вернётся ошибка
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question':question})


def results(request, question_id):
    return HttpResponse(f'Looking at question {question_id} results.')

def vote(request, question_id):
    return HttpResponse(f'Voting on question {question_id}.')