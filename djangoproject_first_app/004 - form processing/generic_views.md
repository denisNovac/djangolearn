# Меньше кода - лучше

Представления detail() и results() очень просты и похожи.

```python
def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question':question})


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question':question})
```

Также эти представления отображают довольно частую функцию в веб-разработке - получить данные из базы данных по параметру в URL, загрузить шаблон и вернуть его рендер. Из-за частоты использования в Django используется generic views система.

Generic Views (Обобщенные Представления) абстрагируют часто используемые паттерны до такой степени, что иногда даже не нужно писать Python код, чтобы организовать приложение.

Далее мы сконвертируем наше приложение polls, используя обобщённые представления. Нам нужно:

- Изменить URLconf;
- Удалить ненужные представления;
- Познакомиться с обобщёнными представлениями.

## Изменить URLconf

Мы уже умеем подставлять в urlpatterns переменные (вроде question_id). Однако, мы можем использовать этот механизм умнее:

```python
urlpatterns = [
    path('',views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
```

Во втором и третьем путях question_id сменилось на pk.

## Изменить представления

Теперь мы удалим index, detail и results и будем использовать Django generic views. Теперь polls/views.py выглядит следующим образом:

```python
from django.views import generic

class IndexView(generic.ListView):
    template_name='polls/index.html'
    context_object_name='latest_question_list'

    def get_queryset(self):
        return Question.objects.order_by('-pub_date')[:5]


class DetailView (generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'

class ResultsView(generic.DetailView):
    model=Question
    template_name= 'polls/results.html'
```

Мы наследуемся от генериков ListView и DetailView. Это абстракты для двух видов страниц, которые, возможно, захотел бы вывести пользователь - страница со списком объектов и страница с информацей для определённого типа.

- Каждый генерик view должен получить модель, с которой работает. Для этого ему дан атрибут model;

- DetailView исследует primary key, переданный ему переменной pk из запроса, поэтому мы их поменяли.

По дефолту DetailView использует шаблон `<app name>/<model name>_detail.html`. В нашей ситуации он бы затребовал испольования `polls/question_detail.html`. template_name атрибут говорит Django использовать определённый шаблон. 

В предыдущих частях туториала, шаблоны предоставлялись представлениям вместе с контекстом. Для DetailView переменная question передана автоматически - через переменную model (Django обзывает переменную просто как question). Однако, для ListView переменная превращается в `question_list`, а у нас она назвалась `latest_question_list`. Поэтому мы делаем переопределение.

## Добьем автоматику Django

Метод get_queryset() выше является переопределённым методом родителя и позволяет настроить, что мы хотим отображать. Ещё можно сделать так:

```python
class IndexView(generic.ListView):
    template_name='polls/index.html'
    context_object_name='latest_question_list'
    queryset = Question.objects.all()
```

Тут мы переопределяем queryset. Мы не можем сказать, как его использовать (последние пять, как раньше), но можем просто передавать его. Тоже работает, клёво.

Наконец, можно поступить ещё проще:

```python
class IndexView(generic.ListView):
    model=Question
    template_name='polls/index.html'
    context_object_name='latest_question_list'
```

Теперь мы передали модель - Question, и ListView автоматически достаёт из неё список и передаёт в шаблон latest_question_list. Наконец, изменим шаблон index.html:

```html
{% if question_list %}
    <ul>
    {% for question in question_list %}
        <li><a href="{% url 'polls:detail' question.id %}">{{ question.question_text }}</a></li>
    {% endfor %}
    </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}
```

И представление:

```python
class IndexView(generic.ListView):
    model=Question
    template_name='polls/index.html'
```

И Django сам разберёт Question на question_list и передаст в шаблон.