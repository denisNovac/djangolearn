# Написание простой формы

Обновим наш `polls/detail.html` и добавим туда элемент `<form>`:

```html
<h1>{{ question.question_text }}</h1>

{% if error_message %}<p><strong>{{ error_message }}</strong></p>{% endif %}

<form action="{% url 'polls:vote' question.id %}" method="post">
{% csrf_token %}
{% for choice in question.choice_set.all %}
    <input type="radio" name="choice" id="choice{{ forloop.counter }}" value="{{ choice.id }}">
    <label for="choice{{ forloop.counter }}">{{ choice.choice_text }}</label><br>
{% endfor %}
<input type="submit" value="Vote">
</form>

```

В этом коде происходит следующее:

- Шаблон отображает radio button для каждого варианта ответа. **value** для каждой radio button ассоциировано через ID выборов от этого вопроса (код в `{% %}`). Это значит, что каждый раз, когда кто-то отправит форму, произойдёт `POST` запрос с  **value** параметром кнопки `choice=#` где # это ID выбранного choice. Так работают HTML-формы.

- Это действие настроено в коде `<form action="{% url 'polls:vote' question.id %}" method="post">`. Мы используем post, ведь форма должна изменить что-то на серверной стороне.

- `forloop.counter` показывает, как много раз тег `for` был вызван в цикле. Таким образом мы назначаем нужной кнопке нужный текст (текущей кнопке для этого витка цикла).

- Раз мы используем POST (а он модифицирует какие-то данные), мы должны помнить о Cross Site Request Forgeries. Это атака на вебсайт, когда неавторизованные команды исполняются от имени пользователя, которому верит веб-приложение. Для защиты Django использует систему токенов csrf. Каждая форма с POST должна иметь тег `{% csrf_token %}`. Без него Django не примет POST.

- В самом начале происходит обработка переменной error_message, которая может быть передана.

## Создание представления, принимающего данные

Теперь нужно обработать представление Vote. При его вызове должно что-то происходить. Пока она отображает лишь строчку, однако, мы хотим, чтобы это представление засчитывало вариант ответа.

```python
def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question':question,
            'error_message': 'You didn\'t select a choice'
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Всегда следует возвращать HttpResoinseRedirect после
        # успешной обработки POST, это позволяет избежать
        # двойного POST, если пользователь нажмёт Back кнопку
        return HttpResponseRedirect( reverse('polls:results', args=(question_id,)))

    return HttpResponse(f'Voting on question {question_id}.')
```

Здесь происходит следующее:

- `request.POST` - это словарь, который позволяет получить запосченные данные. Из шаблона details мы помним, что POST передаёт choice=choice.id, поэтому мы используем содержимое как ключ для БД. requestPost - это всегда string. 

- `request.POST['choice']` бросит ошибку KeyError, если в POST такой ключ задан не был. Кроме того, мы ловим исключение, когда ключ в POST передан, но такого вхождения нет в БД.

- После увеличения votes переменной, мы вовзращаем редирект, а не HttpResponse. HttpResponseRedirect принимает только один аргумент - URL, на который нужно перенаправить. Redirect позволяет скрыть от пользователя представление vote, которое становится обработчиком. Теперь пользователь не попадает напрямую на него, а потому не может повторить результат кнопкой "Назад". Он может это делать для представления с ошибкой, но это ничего не даёт.

- Ещё мы используем reverse() функцию в редиректе. Эта функция позволяет не хардкодить URL в view. В неё передаётся имя представления, которое мы хотим открыть и переменная часть пути до этого представления (question_id). С текущим URLconf reverse вернёт путь вида `polls/3/results`.

Теперь у нас нет доступного представления vote, но нужно закончить представление result:

```python
def results(request, question_id):

    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question':question})
```

```html
<h1>{{ question.question_text }}</h1>

<ul>
{% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }} - {{ choice.votes }} vote{{ choice.votes|pluralize }}</li>
{% endfor %}
</ul>

<a href="{% url 'polls:detail' question.id %}">Vote again?</a>

```

Представление results передаёт в шаблон question. В шаблоне из него достаётся текст и количество голосов для каждого варианта ответа.

Кстати, в vote() существует проблема. Он получает количество голосов, прибавляет один и сохраняет без блокировок. Это значит, что если два пользователя одновременно проголосуют - они оба увидят одно и то же число, хотя одно должно быть больше на один. Это называется race conditions.

