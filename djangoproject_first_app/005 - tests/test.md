# Тесты

Тесты - это простые задачи, которые проверяют код.

Тесты производятся на разных уровнях. Какие-то проверяют небольшую деталь (возвращает ли определенный метод модели ожидаемый результат?), а другие проверяют множество операций сразу (делает ли несколько вводов пользователя ожидаемый результат?). 

Это похоже на использование api из shell, как это было в туториале 2. 

Автоматические тесты предполагают самостоятельное тестирование системой. Вы создаете набор тестов единожды, а затем после смены состояний приложения проверяете, что результаты не изменились. 

## Зачем тесты?

На данном этапе можно подумать, что вам и без тестов достаточно изучения Python и Django, и ещё одну вещь учить и делать - это слишком много и вообще не нужно. Однако, если вы не планируете останавливаться на приложении с опросом - тесты понадобятся.

### Тесты сэкономят вам время

В сложных приложениях может быть множетсво взаимодействий между компонентами. И изменение в одном из них может изменить что-то ещё. Проверка что "вроде работает" двадцатью вариантами решений каждый раз может занимать продолжительное время.

Тесты могут сделать такие проверки за секунды, ещё и помочь выявить, где сломалось.

### Тесты не только идентифицируют проблемы, а ещё и предотвращают

Без тестов иногда невозможно понять, что вообще происходит. Тесты меняют это и подсвечивают код изнутри, а когда что-то идёт не так - они выявляют и это, даже если вы сами не заметили, что что-то сломалось.

### Наконец

Тесты делают код красивее и привлекательнее. Другие программисты иногда сначала смотрят на тесты, чтобы понять, что приложение делает. Тесты помогают работать командам вместе.

# Базовые стратегии

Некоторые программисты следуют дисциплине "test-driven development". Они пишут сначала тесты, а потом код. Суть тут в том, что сначала они как бы описывают ожидаемое решение проблемы, а потом пытаются его исполнить. 

Но новички обычно сначала пишут код, а потом тесты. Возможно, лучше писать тесты заранее, но писать их никогда не поздно.

## Наш первый тест

Вот это везение, у нас уже есть баг в приложении polls. Question.was_publichsed_recently() возвращет True если Question опубликован в будущем! Используем shell, чтобы проверить баг:

```python
>>> import datetime
>>> from django.utils import timezone
>>> from polls.models import Question
>>> future_question = Question(pub_date=timezone.now()+datetime.timedelta(days=30))
>>> future_question.was_published_recently()
True
```

Будущее не "недавно", поэтому это неверно.

## Создаём тест

Что мы сделали в консоли - это то, что мы можем сделать в автотесте.

По соглашениям тесты обычно размещают в файле tests.py. Тестирующая система автоматически найдет тесты (любой файл, начинающийся с test). Создадим файл в polls/tests.py:

```python
import datetime

from django.test import TestCase
from django.utils import timezone

from .models import Question

class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() returns False for questions whose
        pub_date is in future
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)
```

Теперь у нас есть наследник django.test.TestCase с методом, который создаёт Question инстанс с датой в будущем. Затем мы проверяем вывод функции, который должен давать False.

## Запуск тестов

Тесты запускаются через `manage.py test polls`:

```bash
python djangoproject_first_app/005\ -\ tests/mysite/manage.py test polls

Creating test database for alias 'default'...
System check identified no issues (0 silenced).
F
======================================================================
FAIL: test_was_published_recently_with_future_question (polls.tests.QuestionModelTests)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/home/denis/dev/djangolearn/djangoproject_first_app/005 - tests/mysite/polls/tests.py", line 17, in test_was_published_recently_with_future_question
    self.assertIs(future_question.was_published_recently(), False)
AssertionError: True is not False

----------------------------------------------------------------------
Ran 1 test in 0.001s

FAILED (failures=1)
Destroying test database for alias 'default'...
```

Что произошло:

- manage.py поискал тесты в приложении polls;

- нашёл подкласс TestCase;

- создал БД для тестов;

- нашёл методы, начинающиеся со слова test;

- в методе создал инстанс с 30 днями вперёд;

- и проверил, что выдаёт функция.

## Починка бага

Починить баг довольно легко, нужно просто добавить проверку в models.py:

```python
def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
```

Проверим тест снова:

```bash
python djangoproject_first_app/005\ -\ tests/mysite/manage.py test polls

Creating test database for alias 'default'...
System check identified no issues (0 silenced).
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Destroying test database for alias 'default'..
```

## Более сложные тесты

Будет обидно починкой одного бага сломать что-то ещё. Нужно добавить два теста, чтобы более полно протестировать метод:

```python

def test_was_published_recently_with_old_question(self):
        """
        was_published_recently() returns False for questions whose
        pub_date is within the last day
        """
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)

def test_was_published_recently_with_recently_question(self):
    """
    was_published_recently() returns False for questions whose
    pub_date is within the last day
    """
    time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
    recent_question = Question(pub_date=time)
    self.assertIs(recent_question.was_published_recently(), True)

```

Теперь у нас есть по тесту для будущего, прошлого и сегодняшнего вопросов. Мы полностью покрыли тестами этот метод.

# Тестируем представление

Приложение опросов довольно доверчивое. Оно отобразит любой вопрос, даже если его дата в будущем. Нужно это исправить. Для вопросов с датой публикации в будущем вопрос не должен отображаться, пока эта дата не наступит.

## Тест для представления

Когда мы исправили баг ранее, мы написали тест и затем исправили его. Это был простой пример test-driven девелопмента, но, на самом деле, не важно, в каком порядке это делать. 

В первом тесте мы фокусировались на внутреннем поведении. Но для этого теста нужно проверить то, как пользователь увидит результат в браузере.

# Django test client

Django предоставляет клиент для тестов, чтобы симулировать пользовательские интеракции с кодом на уровне представления. Его можно использовать через shell или в test.py. В shell вводим:

```python
>>> from django.test.utils import setup_test_environment
>>> setup_test_environment()
```

Этот метод устанавливает шаблон рендерер, который позволяет проверять некоторые нестандартные аттрибуты вроде `response.context`. Этот метод **не устанавливает тестовую базу данных**, так что всё последующее будет происходить в существующей базе данных и вывод может немного отличаться в зависимости от того, что уже есть в базе. 

Далее импортируем класс клиента (он входит в TestCase, так что потом не придётся):

```python
>>> from django.test import Client
>>> client=Client()
```

И начинаем работать с ним:

```python
>>> response=client.get('/')
Not Found: /
>>> response.status_code
404
```
Отсюда мы ожидаем увидеть 404. И это так.


```python
>>> from django.urls import reverse
>>> response = client.get(reverse('polls:index'))
>>> response.status_code
200
>>> response.content
b'\n    <ul>\n    \n        <li><a href="/polls/1/">What&#39;s new folks?</a></li>\n    \n        <li><a href="/polls/3/">\xd0\xa4\xd0\xb8\xd0\xbb\xd0\xb8\xd0\xbf\xd0\xbf \xd0\xbf\xd0\xb8\xd0\xb4\xd0\xbe\xd1\x80?</a></li>\n    \n    </ul>\n\n\n'
>>> response.context['question_list']
<QuerySet [<Question: What's new folks?>]>
```

А вот в `/polls/` что-то быть должно. И так и оказалось

## Улучшим наше представление

Мы используем ListView в нашем представлении:

```python
class IndexView(generic.ListView):
    model=Question
    template_name='polls/index.html'
```

Через метод get_queryset(self) перестанем выдавать вопросы, которые должны появиться в будущем:

```python
class IndexView(generic.ListView):
    template_name='polls/index.html'
    def  get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now())
```

Теперь мы возвращаем только опросы, чья дата публикации меньше или равна timezone.now().

## Пишем тесты для представления IndexView

В файле polls/tests.py добавим:

```python
def create_question(question_text, days):
    """
    Создать вопрос с текстом и датой, сдвинутой от сегодня на 
    переменную days
    """
    time = timezone.now()+datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)

class QuestionIndexViewTests(TestCase):
    
    def test_no_questions(self):
        """
        Если не существует опросов, должно появиться сообщение.
        """
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'No polls are available.')
        self.assertQuerysetEqual(response.context['question_list'], [])

    def test_past_question(self):
        """
        Вопросы с датой публикации в прошлом отображены на странице.
        """
        create_question(question_text='Past question.', days=-30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['question_list'], 
                                ['<Question: Past question.>'])
        
    def test_future_question(self):
        """
        Вопросы с датой публикации в будущем не отображаются
        """
        create_question(question_text='Future question.', days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, 'No polls are available.')
        self.assertQuerysetEqual(response.context['question_list'], [])

    def test_future_question_and_past_question(self):
        """
        Даже если существуют и вопросы из будущего, и вопросы из прошлого,
        отображаться должны только вопросы из прошлого
        """

        create_question(question_text='Past question.', days=-30)
        create_question(question_text='Future question.', days=30)

        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(response.context['question_list'], 
                                ['<Question: Past question.>'])

    def test_two_past_questions(self):
        """
        Главная страница должна отображать несколько вопросов
        """
        create_question(question_text='Past question 1.', days=-30)
        create_question(question_text='Past question 2.', days=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(len(response.context['question_list']), 2)
```

Стоит заметить, что для каждого вызываемого метода БД сбрасывается.

## Тестируем DetailView

Теперь тесты из будущего не появляются на главной страничке. Однако, к ним можно прийти через DetailView (в строке браузера вписать). Нужно это поправить:

```python
class DetailView (generic.DetailView):
    #model = Question
    template_name = 'polls/detail.html'
    def get_queryset(self):
        """
        Исключает вопросы из будущего.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())
```

Затем можно написать тесты:

```python
class QuestionDetailViewTests(TestCase):

    def test_future_question(self):
        """
        Вопросы с датой публикации в будущем возвращают 404
        """
        future_question = create_question(question_text='Future question.', days=30)
        url = reverse('polls:detail', args=(future_question.id, ))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        """
        Вопросы с датой публикации в прошлом отображаются
        """
        past_question = create_question(question_text='Future question.', days= -5)
        url = reverse('polls:detail', args=(past_question.id, ))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)
```

# Идеи для других тестов

Для ResultView также можно добавить ограничение на просмотр будущих опросов, а потом добавить к этому тест. Он будет очень похож на тесты для DetailView. 

Ещё глупо, что можно публиковать опросы без вариантов ответов, и это тоже можно проверить и улучшить. Тесты бы создавали опрос и проверяли, отобразился он или нет.

В итоге - что угодно, что должно быть добавлено в ПО, должно сопровождаться тестом. 

Однажды ты посмотришь на свои тесты и удивишься, почему твой код тонет в тестах, что приводит нас к...

# Для тестов чем больше - тем лучше

Может показаться, что тесты выходят из под контроля. Однажды кода для тестов станет больше, чем для приложения, а повторы неэстетичные по сравнению с остальным кодом. 

**Ну и пусть**. Для большей части вещей можно написать тест один раз и забыть. 

Иногда тесты нужно обновлять. И когда мы изменили наше приложение, сломанные тесты сами подскажут, что нужно отредактировать. Тесты помогают следить за собой сами!

В тестировании **избыточность** - это хорошо. Главное - сохранять структурированность.

# Дальнейшее тестирование

Этот туториал только про базовые основы тестирования. Например, в Django есть инструменты для тестирования со Selenium.

Для большого и сложного приложения может потребоваться запускать тесты автоматически с каждым коммитом - это называется continuous integration.

Если код нельзя протестировать - обычно это означает, что код плох, и его нужно переписать или убрать вовсе.