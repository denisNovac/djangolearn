# Представляем Django Admin

Философия:

- Генерация админского фронта для персонала или клиентов для добавления, изменения и удаления контента - это однообразная работа, которая не требует много креатива. Поэтому Django делает это автоматически, создавая админские интерфейсы для моделей.

## Создание пользователя

`python manage.py createsuperuser`

```bash
python manage.py createsuperuser

Username (leave blank to use 'denis'): admin
Email address: admin@example.com
Password: 
Password (again): 
The password is too similar to the username.
This password is too short. It must contain at least 8 characters.
This password is too common.
Bypass password validation and create user anyway? [y/N]: y
Superuser created successfully.
```

Запуск сервера:

`python manage.py runserver`

Консоль админа находится по пути 127.0.0.1:8000/admin/

![admin_console](images/001-admin_console.jpg)

Как видно, в консоли практически ничего нет. 

## Добавление контента для администрирования

Для добавления контента для администрирования нужно отредактировать файл polls/admin.py. 

```python
from django.contrib import admin

from .models import Question
# Register your models here.

admin.site.register(Question)
```

После сохранения страничка обновится автоматически.

![admin_console_polls](images/002-admin_console_polls.jpg)

Окно с Question собирается автоматически исходя из модели:

![question](images/003-question.jpg)

Добавим Choice тем же образом. Вот так выглядит окно администрирования выборов:

![choice](images/004-choice.jpg)

