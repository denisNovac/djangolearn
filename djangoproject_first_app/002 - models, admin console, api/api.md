# Поиграться с АПИ

API Django предоставляет интерактивную оболочку.

`python manage.py shell`

Она отличается от оболочки, получаемой при запуске просто `python`, потому что manage.py передаёт переменную DJANGO_SETTINGS_MODULE, которая импортирует путь к моему mysite/settings.py.

Существует API для баз данных. Для этого нужно импортировать модели:

```python
>>>from polls.models import Choice, Question
```

Затем можно вывести список сущностей:

```python
>>> Choice.objects.all()
<QuerySet []>
>>> Question.objects.all()
<QuerySet []>
```

Можно даже руками создать сущность и вложить в БД:

```python
>>> from django.utils import timezone
>>> q = Question(question_text="What's new?", pub_date=timezone.now())
>>> q.save()
>>>q.id
1
>>> Question.objects.all()
<QuerySet [<Question: Question object (1)>]>
```

# Представление объектов

Как видно из `Question.obejcts.all()`, представление довольно неудобное. Можно переопределить метод str, чтобы сделать вывод в консоль красиво:

```python
from django.db import models

class Question( models.Model ):
    
    #...
    
    def __str__(self):
        return self.question_text

class Choice( models.Model ):
    
    #...

    def __str__(self):
        return self.choice_text
```

Этот метод также используется автоматически-сгенерированным Django admin-ом, поэтому может оказаться удобным не только для консольного использования.

А ещё можно добавить такой метод для Question:

```python
import datetime
from django.db import models
from django.utils import timezone

class Question( models.Model ):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return self.question_text
```

Этот метод возвращает True, если вопрос был опубликован сегодня. Теперь перезапустим оболочку.

```python
>>> from polls.models import Choice, Question
>>> Question.objects.all()
<QuerySet [<Question: What's new?>]>
>>> q = Question.objects.get(pk=1)
>>> q.was_published_recently()
True
```

Есть ещё  фильтры:

```python
>>> Question.objects.filter(id=1)
<QuerySet [<Question: What's new?>]>
>>> Question.objects.filter(question_text__startswith='What')
<QuerySet [<Question: What's new?>]>
```

Отобразить все варианты ответов для вопроса:

```python
>>> q.choice_set.all()
<QuerySet []>
```

Добавим варианты ответов:

```python
>>> q.choice_set.create(choice_text='Not much', votes=0)
<Choice: Not much>
>>> q.choice_set.create(choice_text='The sky', votes=0)
<Choice: The sky>
>>> q.choice_set.create(choice_text='Just hacking again', votes=0)
<Choice: Just hacking again>
>>> q.choice_set.all()
<QuerySet [<Choice: Not much>, <Choice: The sky>, <Choice: Just hacking again>]>
q.save()
```

Удалим ответ:

```python
>>> c = q.choice_set.filter(choice_text__startswith='The')
>>> c.delete()
(1, {'polls.Choice': 1})
>>> q.save()
```

