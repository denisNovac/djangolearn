# Настройка базы данных

Теперь следует открыть каталог `mysite/settings.py`. Это модуль, отображающий настройки Django в общем.

По дефолту, конфигурация использует SQLite. Это простейший выбор для новичка. SQLite также является частью Python. Для реального проекта может потребоваться что-нибудь вроде PostgreSQL.

Чтобы настроить БД, нужно прибегнуть к настройке следующей части кода:

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
```

Движок указывается в поле ENGINE. Это может быть любой коннектор:

```python
'django.db.backends.sqlite3', 'django.db.backends.postgresql', 'django.db.backends.mysql', or 'django.db.backends.oracle'
```

NAME - это имя базы данных. В данном случае используется файл db.sqlite3 в корне проекта.

Если использовать что-то кроме sqlite, может потребоваться указать поля USER, HOST, PASSWORD. Кроме того, должна быть создана база данных внутри схемы. Кроме того, юзер должен иметь привилегии create database. Это позволит автоматически создавать test database.

В settings.py следует выставить TIME_ZONE:

```python
TIME_ZONE = 'Asia/Yekaterinburg'
```

Есть блок INSTALLED_APPS:

```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

Это приложения, по дефолту поставляемые с Django.
Некоторые требуют хотя бы одной таблицы в БД, поэтому таблицы нужно создать.

# Создание таблиц

`python manage.py migrate`

Вывод:

```
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying sessions.0001_initial... OK
```

Команда migrate просматривает список INSTALLED_APPS и создаёт нужные таблицы в базе данных, указанной в конфиге.

Созданные таблицы:

```sql
sqlite3 -cmd .schema db.sqlite3 | grep 'CREATE TABLE'

CREATE TABLE IF NOT EXISTS "django_migrations" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "app" varchar(255) NOT NULL, "name" varchar(255) NOT NULL, "applied" datetime NOT NULL);
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE IF NOT EXISTS "auth_group_permissions" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "group_id" integer NOT NULL REFERENCES "auth_group" ("id") DEFERRABLE INITIALLY DEFERRED, "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE TABLE IF NOT EXISTS "auth_user_groups" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "user_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED, "group_id" integer NOT NULL REFERENCES "auth_group" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE TABLE IF NOT EXISTS "auth_user_user_permissions" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "user_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED, "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE TABLE IF NOT EXISTS "django_admin_log" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "action_time" datetime NOT NULL, "object_id" text NULL, "object_repr" varchar(200) NOT NULL, "change_message" text NOT NULL, "content_type_id" integer NULL REFERENCES "django_content_type" ("id") DEFERRABLE INITIALLY DEFERRED, "user_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED, "action_flag" smallint unsigned NOT NULL CHECK ("action_flag" >= 0));
CREATE TABLE IF NOT EXISTS "django_content_type" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "app_label" varchar(100) NOT NULL, "model" varchar(100) NOT NULL);
CREATE TABLE IF NOT EXISTS "auth_permission" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id") DEFERRABLE INITIALLY DEFERRED, "codename" varchar(100) NOT NULL, "name" varchar(255) NOT NULL);
CREATE TABLE IF NOT EXISTS "auth_user" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "password" varchar(128) NOT NULL, "last_login" datetime NULL, "is_superuser" bool NOT NULL, "username" varchar(150) NOT NULL UNIQUE, "first_name" varchar(30) NOT NULL, "email" varchar(254) NOT NULL, "is_staff" bool NOT NULL, "is_active" bool NOT NULL, "date_joined" datetime NOT NULL, "last_name" varchar(150) NOT NULL);
CREATE TABLE IF NOT EXISTS "auth_group" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(150) NOT NULL UNIQUE);
CREATE TABLE IF NOT EXISTS "django_session" ("session_key" varchar(40) NOT NULL PRIMARY KEY, "session_data" text NOT NULL, "expire_date" datetime NOT NULL);
```

Вообще, дефолтные приложение включены для обычного пользования, но не каждому они нужны. Их можно удалять из INSTALLED_APPS перед запуском migrate. 

# Создание моделей

Модель - это макет базы данных.

Модель - это единственный источник данных о нашей информации. Она содержит важнейшие поля и поведения данных, которые хранятся. Django использует принцип DRY. Основная идея тут - определить модель данных в одном месте и автоматически получать из неё данные. 

Для нашего сайта требуется две модели: Question и Choice. 

- Question включает в себя вопрос и дату публикации.
- Choice включает поле text (сам вариант выбора) и количество его выборов.

Каждый выбор ассоциирован с некоторым вопросом.

Эти концепты можно реализовать в простых Python классах. Для этого используется файл polls/models.py:

```python
from django.db import models

class Question( models.Model ):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

class Choice( models.Model ):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0
```

Видно, что это ООП-основа для создания таблиц БД.

- Каждая модель представлена классом, который наследует класс Model.
- Каждая модель имеет несколько полей, представляющих поля в базе данных.
- Каждое поле представлено некоторым экземпляром Field - CharField или DateTimeField в примере выше (question - тоже как бы поле Field, но оно ссылается на записи Question).
- Имя каждого поля (question_text и pub_date выше) - это имя в машинном формате. Это знаение будет использовано в Python коде, а наша БД будет использовать его как имя колонки. Можно использовать опционально первый аргумент как текстовое имя колонки. В примере выше оно есть только у pub_date.

Некоторые Field требуют аргументов. Например, CharField хочет длину строки. 

Некоторые Field могут иметь опциональные аргументы. Например, default для IntegerField.

Наконец, ForeignKey указывает на отношение. Это значит, что каждая запись Choice относится к какому-то Question. 

# Применение модели

С этими моделями Django поступит следующим образом:

- Создаст схему БД (CREATE TABLE) для этого приложения;
- Создаст Python API для доступа к этим объектам.

Но сначала нужно добавить наше приложение в настройки settings.py для правильных миграций. Чтобы сделать это, нужно добавить в поле INSTALLED_APPS класс из polls/apps.py, отвечающий за настройки.

Код polls/apps.py:

```python
from django.apps import AppConfig


class PollsConfig(AppConfig):
    name = 'polls'

```

Полученный INSTALLED_APPS:

```python
INSTALLED_APPS = [
    'polls.apps.PollsConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

Теперь можно запустить генерацию миграций для polls:

`python manage.py makemigrations polls`

Вывод:

```
Migrations for 'polls':
  polls/migrations/0001_initial.py
    - Create model Question
    - Create model Choice
```

Команда `makemigrations` используется когда сделаны некоторые изменения в моделях и нужно сохранить их как миграции. Команда makemigrations **не запускает сами миграции, только генерирует!**

Миграции - это то, как Django хранит изменения в моделях (и твоя БД схема) - это просто файлы на диске. Файлы миграций лежат в polls/migrations/ . Файлы оттуда можно запустить автоматически командной migrate, а посмотреть командой sqlmigrate (0001 - первая миграция в папке):

```sql
$ python manage.py sqlmigrate polls 0001
BEGIN;
--
-- Create model Question
--
CREATE TABLE "polls_question" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "question_text" varchar(200) NOT NULL, "pub_date" datetime NOT NULL);
--
-- Create model Choice
--
CREATE TABLE "polls_choice" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "choice_text" varchar(200) NOT NULL, "votes" integer NOT NULL, "question_id" integer NOT NULL REFERENCES "polls_question" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE INDEX "polls_choice_question_id_c5b4b260" ON "polls_choice" ("question_id");
COMMIT;


```

Замечания:

- Выводы зависят от БД, которая используется;

- Имена таблиц генерируются автоматически из имени приложения и модели;

- Первичные ключи тоже генерируются автоматически;

- INDEX в SQLite, видимо, отвечает за внешний ключ;

- sqlmigrate ничего не запускает. просто показывает то, что будет запущено.

Команда `python manage.py migrate` **выполняет все сгенерированные миграции**, которые ещё не были запущены. Примерный вывод (да, для отдельного приложения по конкретной миграции):

```
python manage.py migrate polls 0001
Operations to perform:
  Target specific migration: 0001_initial, from polls
Running migrations:
  Applying polls.0001_initial... OK
```


Django следит за этим, используя таблицу django_migrations. 

Миграции позволяют менять модели на протяжении времени без нужды удалять базу данных. Они специализируются на апгрейде бд наживую без потери данных. 

Последовательность действий:

- Сгенерировать миграции manage.py makemigrations;

- Выплнить миграции manage.py migrate.
