# Установка

Django - это фреймворк Python. Поэтому ему нужен Python. 
- Python имеет встроенную SQLite, так что пока новую БД можно не ставить.

Django можно установить через pip:

`pip install Django`

Проверить можно программой:

```python
import django
print(django.get_version())
```

